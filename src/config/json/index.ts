import { CRM } from './crm_enums.json';
import {
	DATA_KEYS as DATA,
	VALIDATION_KEYS
}              from './bitrix.json';

const {
	CARGO,
	CARGOINN,
	DRIVER,
	ORDER,
	PAYMENT,
	TRANSPORT
} = DATA

export {
	CRM,
	VALIDATION_KEYS,
	CARGO,
	CARGOINN,
	DRIVER,
	ORDER,
	PAYMENT,
	TRANSPORT,
};
